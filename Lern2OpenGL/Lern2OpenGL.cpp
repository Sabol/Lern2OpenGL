// Lern2OpenGL.cpp : Defines the entry point for the console application.
//

//#define SDL_MAIN_HANDLED // hack to getaround SDL's main

#include "stdafx.h"
#include <SDL2\SDL.h>
#include <iostream>
#include "Display\Display.h"

/*
Just a test bed using SDL2, glew, and glm to learn how to use OpenGL and maek gaem.
*/


int main( int argc, char* argv[] ) {
	Display win( 800, 600, "Lern2OpenGL" );

	// We gonna draw a triangle
	GLfloat verts[] = {
		-0.5f, -0.5f, 0.0f,
		 0.5f, -0.5f, 0.0f,
		 0.0f,  0.5f, 0.0f
	};

	// A vertex shader
	// Need to create a shader class to easily load shader files into a single GLchar* string
	const GLchar* vert_shadersrc = "#version 330 core\n"
		"layout (location = 0) in vec3 position;\n"
		"void main() {\n"
		"gl_Position = vec4(position.x, position.y, position.z, 1.0);\n"
		"}\0";
	GLuint vertex_shader;
	vertex_shader = glCreateShader( GL_VERTEX_SHADER );
	glShaderSource( vertex_shader, 1, &vert_shadersrc, NULL );
	glCompileShader( vertex_shader );
	GLint success;
	GLchar infoLog[512];
	glGetShaderiv( vertex_shader, GL_COMPILE_STATUS, &success );

	if ( !success ) {
		glGetShaderInfoLog( vertex_shader, 512, NULL, infoLog );
		std::cerr << "ERROR::SHADER::VERTEX:COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// A fragment shader
	const GLchar* frag_shadersrc = "#version 330 core\n"
		"out vec4 color;\n"
		"void main() {\n"
		"color = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
        "}\0";
	GLuint fragment_shader = glCreateShader( GL_FRAGMENT_SHADER );
	glShaderSource( fragment_shader, 1, &frag_shadersrc, NULL );
	glCompileShader( fragment_shader );
	glGetShaderiv( fragment_shader, GL_COMPILE_STATUS, &success );

	if ( !success ) {
		glGetShaderInfoLog( fragment_shader, 512, NULL, infoLog );
		std::cerr << "ERROR::FRAGMENT::SHADER::COMPLIATION_FAILED\n" << infoLog << std::endl;
	}

	// Shader program
	GLuint shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader( shaderProgram, vertex_shader );
	glAttachShader( shaderProgram, fragment_shader );
	glLinkProgram( shaderProgram );

	glGetProgramiv( shaderProgram, GL_LINK_STATUS, &success );
	if ( !success ) {
		glGetProgramInfoLog( shaderProgram, 512, NULL, infoLog );
		std::cerr << "ERROR::SHADER_PROGRAM::LINK_ERROR\n" << infoLog << std::endl;
	}

	// Use the linked shader program
	glUseProgram( shaderProgram );
	// delete the shaders, we don't need them anylonger
	glDeleteShader( fragment_shader );
	glDeleteShader( vertex_shader );

	// Create a VBO (vertex buffer object)
		
	/*glBindBuffer( GL_ARRAY_BUFFER, VBO );
		glBufferData( GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW );
		// 1. set the vertex attrubutes pointers
		glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0 );
		glEnableVertexAttribArray( 0 );
		// 2. use our shader program when we want to render an object
		glUseProgram( shaderProgram );
		// 3. draw the object
    */

	// VAO
	GLuint VAO, VBO;
	// 0. copy vertice date arr in a buffer for OpenGL to use
	glGenBuffers( 1, &VBO );
	glGenVertexArrays( 1, &VAO );
	// 1. Bind vertex Array Object
	glBindVertexArray( VAO );
		// 2. copy verts array in buffer for opengl to use
		glBindBuffer( GL_ARRAY_BUFFER, VBO );
		glBufferData( GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW );
		// 3. set the vertex attribs pointers
		glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0 );
		glEnableVertexAttribArray( 0 ); // don't forget this!

	// 4. unbind the VAO
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindVertexArray( 0 );
	

	// Simple game loop
	while ( win.IsRunning() ) {
		// clear the display
		win.Clear( 0.2f, 0.3f, 0.3f, 1.0f );

		// Drawing code
		// 5. Draw the object (vert data)
		glUseProgram( shaderProgram );
		glBindVertexArray( VAO );
		// someopenglfuncthatdrawsthetriangle()
		glDrawArrays( GL_TRIANGLES, 0, 3 );
		glBindVertexArray( 0 );

		win.Update();

		// Check for some events to exit
		for ( SDL_Event e; SDL_PollEvent(&e); ) {
			switch ( e.type ) {
			case SDL_KEYUP:
				switch ( e.key.keysym.sym ) {
				case SDLK_ESCAPE:
					// close the display
					std::cout << "escape pressed" << std::endl;
					// exit the app
					win.SetRunning( false );
				}
			case SDL_QUIT:
				// need to close the display
				win.SetRunning( false );
			}
		}
	}
    return 0;
}

