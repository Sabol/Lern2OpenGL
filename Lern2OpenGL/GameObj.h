#pragma once
#include <glm\glm.hpp>
#include <string>

class GameObj {
private:
	// Use glm vec3 for position?
	// should be a pointer? nah we like data encapsulation for this
	// pointer if we need to be super memory conscientious.
	glm::vec3   position;
	std::string tag; // tag that is user set to help identify the object aside from it's type. E.G enemy

public:
	GameObj();
	virtual ~GameObj();
	
	inline glm::vec3 GetPos() { return position; }
	inline glm::vec3 SetPos( glm::vec3 position ) { this->position = position; }
};

