#include "Display.h"

Display::Display( int width, int height, const std::string& title ) {
	
	InitSDL();
	window = SDL_CreateWindow(
		title.c_str(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		width,
		height,
		SDL_WINDOW_OPENGL
	);
	glContext = SDL_GL_CreateContext( window );
	if ( glContext == NULL ) {
		std::cerr << "Error: could not create gl context." << std::endl;
	}
	InitGLEW();
	//glViewport( 0, 0, width, height );
}

Display::~Display() {
	SDL_GL_DeleteContext( glContext );
	SDL_DestroyWindow( window );
	SDL_Quit();
}

void Display::InitSDL() {
	// Start SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		std::cerr << "SDL failed to initialize!" << std::endl;
		running = false;
	}
	// Set some GL attributes to start off with
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
}

void Display::InitGLEW() {
	// Make sure GLEW uses more modern techniques
	glewExperimental = GLU_TRUE;
	const GLenum result = glewInit();
	if ( result != GLEW_OK ) {
		std::cerr << "Error: could not initialize GLEW." << std::endl;
		running = false;
	}
}

void Display::Update() {
	// double buffer
	SDL_GL_SwapWindow( window );
}

void Display::Clear( GLfloat r, GLfloat g, GLfloat b, GLfloat a ) {
	glClearColor( r, g, b, a );
	glClear( GL_COLOR_BUFFER_BIT );
}
