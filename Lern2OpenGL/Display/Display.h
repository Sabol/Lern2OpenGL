#pragma once

#include <SDL2\SDL.h>
#include <iostream>
#include <string>
#include <GL\glew.h>

class Display {
private:
	bool running;
	SDL_Window  * window;
	SDL_GLContext glContext;

public:
	Display( int width, int height, const std::string& title );
	virtual ~Display();

	void InitSDL();
	void InitGLEW();
	void Update();
	void Clear( GLfloat r, GLfloat g, GLfloat b, GLfloat a );
	inline bool IsRunning() { return running; };
	inline void SetRunning(bool running) { this->running = running; }
};

